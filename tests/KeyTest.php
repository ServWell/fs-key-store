<?php namespace Tests;

use PHPUnit\Framework\TestCase;
use ServWell\FsKeyStore\Exception\FileSystemException;
use ServWell\FsKeyStore\Model\KeyValuePair;
use ServWell\FsKeyStore\Model\Scalar\Boolean_;
use ServWell\FsKeyStore\Model\Scalar\Float_;
use ServWell\FsKeyStore\Model\Scalar\Integer_;
use ServWell\FsKeyStore\Model\Scalar\String_;
use Tests\Factories\KeyValuePairFactory;
use Tests\Factories\StoreFactory;

class KeyTest extends TestCase
{
    /**
     * @throws FileSystemException
     * @throws \ServWell\FsKeyStore\Exception\KeyException
     * @throws \ServWell\FsKeyStore\Exception\ScalarException
     */
    public function testKeyWithFloatValueCanBeSetAndRetrieved()
    {
        $keyValuePair = KeyValuePairFactory::getKeyValuePairWithValueAsFloat();
        $this->assertScalarMatches($keyValuePair, Float_::SCALAR_NAME);
    }

    /**
     * @throws FileSystemException
     * @throws \ServWell\FsKeyStore\Exception\KeyException
     * @throws \ServWell\FsKeyStore\Exception\ScalarException
     */
    public function testKeyWithIntegerValueCanBeSetAndRetrieved()
    {
        $keyValuePair = KeyValuePairFactory::getKeyValuePairWithValueAsInt();
        $this->assertScalarMatches($keyValuePair, Integer_::SCALAR_NAME);
    }

    /**
     * @throws FileSystemException
     * @throws \ServWell\FsKeyStore\Exception\KeyException
     * @throws \ServWell\FsKeyStore\Exception\ScalarException
     */
    public function testKeyWithBooleanValueCanBeSetAndRetrieved()
    {
        $keyValuePair = KeyValuePairFactory::getKeyValuePairWithValueAsBoolean();
        $this->assertScalarMatches($keyValuePair, Boolean_::SCALAR_NAME);
    }

    /**
     * @throws FileSystemException
     * @throws \ServWell\FsKeyStore\Exception\KeyException
     * @throws \ServWell\FsKeyStore\Exception\ScalarException
     */
    public function testKeyWithStringValueCanBeSetAndRetrieved()
    {
        $keyValuePair = KeyValuePairFactory::getKeyValuePairWithValueAsString();
        $this->assertScalarMatches($keyValuePair, String_::SCALAR_NAME);
    }

    /**
     * @param KeyValuePair $keyValuePair
     * @param string $scalarType
     * @throws FileSystemException
     * @throws \ServWell\FsKeyStore\Exception\KeyException
     * @throws \ServWell\FsKeyStore\Exception\ScalarException
     */
    protected function assertScalarMatches(KeyValuePair $keyValuePair, string $scalarType)
    {
        ($store = StoreFactory::CreateStoreIn(__DIR__ . '/../tmp/'))
            ->key($keyValuePair->key)->setValue($keyValuePair->value);
        $this->assertTrue(
            (($type = gettype($store->key($keyValuePair->key)->getValue())) === $scalarType),
            "Did not get the same value casted as a $scalarType. Got a $type."
        );
        $this->assertTrue(
            ($keyValuePair->value === $store->key($keyValuePair->key)->getValue()),
            'Did not get the same value back that was stored.'
        );
    }
}
