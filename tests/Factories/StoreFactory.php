<?php namespace Tests\Factories;

use Faker\Factory;
use ServWell\FsKeyStore\Store;

class StoreFactory
{

    /**
     * @param $dir
     * @return Store
     * @throws \ServWell\FsKeyStore\Exception\FileSystemException
     */
    public static function CreateStoreIn($dir): Store
    {
        $faker = Factory::create();
        mkdir($dir = realpath($dir) . '/' . $faker->shuffleString('abcdefghijklmnopqrstuvwxyz-_'), 0777);
        return new Store($dir);
    }
}
