<?php namespace Tests\Factories;

use Faker\Factory;
use ServWell\FsKeyStore\Model\KeyValuePair;

class KeyValuePairFactory
{
    public static function getKeyValuePairWithValueAsFloat(): KeyValuePair
    {
        $faker = Factory::create();
        $keyValuePair = new KeyValuePair();
        $keyValuePair->key = $faker->shuffleString('abcdefghijklmnopqrstuvwxyz-_');
        $keyValuePair->value = (float)(
            $faker->randomNumber(5)
            . '.' .
            $faker->randomNumber(5)
        );
        return $keyValuePair;
    }

    public static function getKeyValuePairWithValueAsInt(): KeyValuePair
    {
        $faker = Factory::create();
        $keyValuePair = new KeyValuePair();
        $keyValuePair->key = $faker->shuffleString('abcdefghijklmnopqrstuvwxyz-_');
        $keyValuePair->value = (int)$faker->randomNumber(5);
        return $keyValuePair;
    }

    public static function getKeyValuePairWithValueAsBoolean(): KeyValuePair
    {
        $faker = Factory::create();
        $keyValuePair = new KeyValuePair();
        $keyValuePair->key = $faker->shuffleString('abcdefghijklmnopqrstuvwxyz-_');
        $keyValuePair->value = ((int)($faker->randomNumber(5)%2) === 0);
        return $keyValuePair;
    }

    public static function getKeyValuePairWithValueAsString(): KeyValuePair
    {
        $faker = Factory::create();
        $keyValuePair = new KeyValuePair();
        $keyValuePair->key = $faker->shuffleString('abcdefghijklmnopqrstuvwxyz-_');
        $keyValuePair->value = $faker->shuffleString('abcdefghijklmnopqrstuvwxyz-_ ');
        return $keyValuePair;
    }
}
