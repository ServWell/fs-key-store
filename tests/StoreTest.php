<?php namespace Tests;

use Exception;
use PHPUnit\Framework\TestCase;
use ServWell\FsKeyStore\Exception\FileSystemException;
use ServWell\FsKeyStore\Store;

class StoreTest extends TestCase {

    public function testStoreThrowsErrorIfDirectoryDoesNotExist()
    {
        $exception = null;
        try{
            new Store('/does/not/exist');
        }
        catch(Exception $e) {

            $exception = $e;
        }

        $this->assertTrue(($exception instanceof FileSystemException));
    }
}
