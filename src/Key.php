<?php namespace ServWell\FsKeyStore;

use ServWell\FsKeyStore\Exception\FileSystemException;
use ServWell\FsKeyStore\Exception\KeyException;
use ServWell\FsKeyStore\Exception\ScalarException;
use ServWell\FsKeyStore\Model\Scalar\Scalar;

class Key
{
    /** @var string The Key of the key/value pair. */
    protected $key;

    /** @var Store The store of this key */
    protected $store;

    /**
     * Creates an instance that can manage the data associated with a key.
     *
     * @param Store $store The store of this key
     * @param string $key The Key of the key/value pair.
     */
    function __construct(Store $store, string $key)
    {
        $this->store = $store;
        $this->key = $key;
    }

    /**
     * Gets the value of a key
     *
     * @return bool|float|int|string
     * @throws FileSystemException If cannot read the file.
     * @throws ScalarException     If the value can not be casted.
     */
    public function getValue()
    {
        return (new Scalar($this->store->readFile($this->store->dir . '/' . $this->key)))->cast()->value;
    }

    /**
     * Sets a key/value pair.
     *
     * @param mixed   $value     The value to store.
     * @param boolean $overwrite The value to store.
     * @throws FileSystemException If fails to write the file to file system.
     * @throws KeyException If the key already exists and should not be overwritten.
     * @throws ScalarException If the value is not a Scalar.
     */
    public function setValue($value, bool $overwrite = true): void
    {
        $value = Scalar::assertIsScalar($value) . '|' . $value;
        if(!$overwrite && $this->exists()) {
            throw new KeyException('Tried to overwrite a value that already exists.');
        }

        $this->store->writeFile($this->store->dir . '/' . $this->key, (string)$value);
    }

    /**
     * Checks if a key exists.
     *
     * @return bool
     */
    public function exists(): bool
    {
        return $this->store->fileExists($this->store->dir . '/' . $this->key);
    }

    /**
     * Delete this key.
     */
    public function delete()
    {
        $this->store->DeleteFile($this->store->dir . '/' . $this->key);
    }
}
