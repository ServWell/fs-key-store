<?php namespace ServWell\FsKeyStore;

use ServWell\FsKeyStore\Exception\FileSystemException;

class FileSystem
{
    /**
     * Asserts directory exists.
     *
     * @param string $path The path of the directory to assert.
     * @return string
     * @throws FileSystemException
     */

    /**
     * @param $path
     * @return string
     * @throws FileSystemException
     */
    public function assertDirectoryExists($path): string
    {
        $path = realpath($path);
        if($path === false) {
            throw new FileSystemException('Storage directory does not exist.');
        }
        elseif(!is_dir($path)) {
            throw new FileSystemException('Storage directory is not a directory.');
        }

        return $path;
    }

    /**
     * Writes a file to the file system.
     *
     * @param string $filename The file name to write to.
     * @param string $data     The data to write to the file.
     * @throws FileSystemException If cannot write to the disk.
     */
    public function writeFile(string $filename, string $data): void
    {
        //die($filename);
        if(file_put_contents($filename, $data) === false) {
            throw new FileSystemException('Failed to write data to the file system.');
        }
    }

    /**
     * Reads a file from the file system.
     *
     * @param string $filename The filename of the file to read.
     * @return string
     * @throws FileSystemException If cannot read the file.
     */
    public function readFile(string $filename): string
    {
        if(($contents = file_get_contents($filename)) === false) {
            throw new FileSystemException('Failed to read data from the file system.');
        }

        return $contents;
    }

    public function fileExists(string $fileName): bool
    {
        return (file_exists($fileName) && !is_dir($fileName));
    }

    public function DeleteFile(string $string)
    {

    }
}
