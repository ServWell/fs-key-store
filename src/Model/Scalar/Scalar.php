<?php namespace ServWell\FsKeyStore\Model\Scalar;

use ServWell\FsKeyStore\Exception\ScalarException;

class Scalar
{
    public $type;
    public $value;

    /**
     * Creates a Scalar instance.
     *
     * @param int|string|boolean|double|float $value The value to parse into this instance.
     */
    function __construct(string $value)
    {
        $this->type = explode('|', $value)[0];
        $this->value = substr($value,mb_strlen($this->type)+1);
    }

    /**
     * Gets an instance of the value's Scalar containing the casted value.
     *
     * @return Scalar
     * @throws ScalarException If the scalar type is unknown.
     */
    public function cast(): self
    {
        switch($this->type) {
            case String_::SCALAR_NAME:
                return new string_($this->value);
                break;
            case Integer_::SCALAR_NAME:
                return new Integer_($this->value);
                break;
            case Float_::SCALAR_NAME:
                return new Float_($this->value);
                break;
            case Boolean_::SCALAR_NAME:
                return new Boolean_($this->value);
                break;
            default:
                throw new ScalarException('Failed to cast scalar because the scalar type is unknown.');
                break;
        }
    }

    /**
     * @param mixed $var The variable to verify.
     * @return string The type of scalar.
     * @throws ScalarException Value($var) is not a scalar.
     */
    public static function assertIsScalar($var): string
    {
        $type = gettype($var);
        if(!is_scalar($var)) {
            throw new ScalarException('Value is not a scalar. It is a ' . $type . '.');
        }

        return $type;
    }
}
