<?php namespace ServWell\FsKeyStore\Model\Scalar;

class Boolean_ extends Scalar
{
    const SCALAR_NAME = 'boolean';

    public $value;

    function __construct($value)
    {
        parent::__construct(self::SCALAR_NAME . '|' . $value);
        $this->value = (bool)$value;
    }
}
