<?php namespace ServWell\FsKeyStore\Model\Scalar;

class Integer_ extends Scalar
{
    const SCALAR_NAME = 'integer';

    public $value;

    function __construct($value)
    {
        parent::__construct(self::SCALAR_NAME . '|' . $value);
        $this->value = (int)$value;
    }
}
