<?php namespace ServWell\FsKeyStore\Model\Scalar;

class Float_ extends Scalar
{
    const SCALAR_NAME = 'double';

    public $value;

    function __construct($value)
    {
        parent::__construct(self::SCALAR_NAME . '|' . $value);
        $this->value = (double)$value;
    }
}
