<?php namespace ServWell\FsKeyStore\Model\Scalar;

class String_ extends Scalar
{
    const SCALAR_NAME = 'string';

    public $value;

    /**
     * String_ constructor.
     * @param $value
     */
    function __construct($value)
    {
        parent::__construct(self::SCALAR_NAME . '|' . $value);
        $this->value = (string)$value;
    }
}
