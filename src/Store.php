<?php namespace ServWell\FsKeyStore;


use ServWell\FsKeyStore\Exception\FileSystemException;

class Store extends FileSystem
{
    var $dir;

    /**
     * Creates a storage instance
     *
     * @param string $dir The directory of the storage system.
     * @throws FileSystemException If the storage directory doesn't exist.
     */
    function __construct($dir)
    {
        $this->dir = $this->assertDirectoryExists($dir);
    }

    /**
     * Gets a key inside a store
     *
     * @param string $key
     * @return Key
     */
    function key(string$key): Key
    {
        return new Key($this, $key);
    }


    /**
     * Creates a storage instance
     *
     * @param string $dir The directory of the storage system.
     * @return Store
     * @throws FileSystemException If the storage directory doesn't exist.
     */
    public static function open($dir)
    {
        return new self($dir);
    }
}
